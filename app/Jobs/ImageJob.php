<?php

namespace App\Jobs;

use App\Models\User;
use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Notifications\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        private User $user,
        private string $status,
        private string $message,
        private object $image,
        private string $filter,
    ) {
        $this->user = $user;
        $this->status = $status;
        $this->message = $message;
        $this->image = $image;
        $this->filter = $filter;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->status == 'failed') {
            $this->user->notify(new ImageProcessingFailedNotification(
                $this->status,
                $this->message,
                $this->image,
                $this->filter,
                $this->user,
            ));
        } elseif ($this->status == 'success') {

            $this->user->notify(new ImageProcessedNotification(
                $this->status,
                $this->image,
                $this->filter,
            ));
        } else {
            throw new \Exception('Status not valid!');
        }
    }
}
