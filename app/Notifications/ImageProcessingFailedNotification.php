<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ImageProcessingFailedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        public string $status,
        public string $message,
        public object $image,
        public string $filter,
        private User $user
    )
    {
        $this->status = $status;
        $this->message = $message;
        $this->image = $image;
        $this->filter = $filter;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            'mail',
            'broadcast'
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Dear' . $this->user->name)
            ->line('The applying the filter "' . $this->filter . '" was failed to the image:')
            ->line('<a href="' . $this->image->getSrc() . '"><img src="' . $this->image->getSrc() . '" alt="Image"></a>')
            ->line('Best regards,')
            ->line('Binary Studio Academy');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'status' => $this->status,
            'message' => $this->message,
            'image' => [
                'id' => $this->image->getId(),
                'src' => $this->image->getSrc(),
            ],
        ];
    }

    /**
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'status' => $this->status,
            'message' => $this->message,
            'image' => [
                'id' => $this->image->getId(),
                'src' => $this->image->getSrc(),
            ],
        ]);
    }

}
