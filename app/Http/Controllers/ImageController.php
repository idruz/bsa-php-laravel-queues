<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Jobs\ImageJob;
use Illuminate\Http\Request;
use App\Values\Image;
use App\Actions\Image\{
    ApplyFilterAction,
    GetFiltersAction,
};
use Illuminate\Support\Facades\Auth;

class ImageController extends Controller
{
    private ApplyFilterAction $applyFilter;
    private GetFiltersAction $getFilters;

    public function __construct(
        ApplyFilterAction $applyFilter,
        GetFiltersAction $getFilters
    ) {
        $this->applyFilter = $applyFilter;
        $this->getFilters = $getFilters;
    }

    public function updateImage(Request $request)
    {
        $image = new Image(
            $request->imageId,
            $request->src,
        );

        $user = Auth::user();
        try {
            $result = $this->applyFilter->execute($image, $request->filter);

            ImageJob::dispatch(
                $user,
                'success',
                '',
                $result->toObject(),
                $request->filter
            )->onConnection('beanstalkd');

            return response()->json(
                $result->toArray()
            );

        } catch (\LogicException $exception) {
            ImageJob::dispatch(
                $user,
                'failed',
                $exception->getMessage(),
                $image,
                $request->filter
            )->onConnection('beanstalkd');

            return response()->json(
                $exception->getMessage(),
                200
            );
        }
    }

    public function filters()
    {
        return response()->json($this->getFilters->execute());
    }
}
